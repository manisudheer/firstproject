const url = "https://www.irctc.co.in/nget/train-search"

module.exports = {
    tags : ['irctc'],
    'Navigating to irctc login page ' : function(browser) {
        browser
        .windowMaximize()
        .url(url)
        .pause(500)
    } ,
    'Alert' : function(browser) {
        browser
        .useXpath().click("//*[contains(@class,'btn btn-primary')]")
        .pause(500)
    },
    'source and destination': function(browser) {
        browser
        .assert.visible("//*[@id='origin']/span/input")
        .setValue("//*[@id='origin']/span/input",'SECUNDERABAD JN - SC')
         .assert.visible("//*[@id='destination']/span/input")
        .setValue("//*[@id='destination']/span/input",'TIRUPATI - TPTY')
        .pause(500)
    },
    'close add' : function(browser) {
        browser
        .useXpath().click("//*[@id='corover-close-btn']")
        .pause(1000)

    },
    'select Journeyclass' : function(browser) {
        browser
        .useXpath().click("//*[@id='journeyClass']/div/label")
        .useXpath().click("//*[@id='journeyClass']/div/div[4]/div/ul/li[7]")
        .pause(500)
    },
    'Find Trains' : function(browser) {
        browser
        .useXpath().click("//*[@id='divMain']/div/app-main-page/div[1]/div/div[1]/div/div/div[1]/div/app-jp-input/div[3]/form/div[7]/button")
        .pause(500)
    },
    'change Journeyclass' : function(browser) {
        browser
        .useXpath().click("//*[@id='ui-accordiontab-0-content']/div/div/form/div[1]/div[3]/p-dropdown/div/div[3]/span")
        .useXpath().click("//*[@id='ui-accordiontab-0-content']/div/div/form/div[1]/div[3]/p-dropdown/div/div[4]/div/ul/li[3]/span")
        .useXpath().click("//*[@id='corover-close-cb-btn']")
        .pause(500)
    },
    'Modify' : function(browser){
        browser
        .useXpath().click("//*[@id='ui-accordiontab-0-content']/div/div/form/div[1]/div[6]/button")
        .pause(500)
    },
    'Train Schedule' : function(browser){
        browser
        .useXpath().click("//*[@id='T_02734']/span")
        .pause(500)
    },
    'close train schedule' : function(browser) {
        browser
        .useXpath().click("//*[@id='divMain']/div/app-train-list/div/div[5]/div/div[2]/div[1]/div[2]/div[5]/div/div[1]/app-train-avl-enq/p-dialog/div/div[1]/a/span")
        .pause(2000)
        .end()
    }
};